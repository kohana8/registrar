const path = require('path');
const Server = require('@komino/fastify-minimal-setup');

const m = new Server(null, {
  salt: '&E)H@McQeOPWmZq4t7w!z%C*F-JaNdRg',
  secureCookie: false,
  maxAge: 86400000,
  sessionDBFolder: path.join(__dirname, '/../db'),
  logger: false,
});

const {K8, Controller} = require('@komino/k8');
K8.init(__dirname);

const {RouteList} = require('@komino/komod-route');
RouteList.createRoute(m.app);

m.listen(8888);
