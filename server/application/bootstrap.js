require('@komino/komod-mixin-orm');
require('@komino/komod-mixin-admin');
require('@komino/komod-route');
require('@komino/komod-route-debug');
require('@komino/komod-liquid-view');

module.exports = {
  modules: [],
  system: []
};

require('./init');