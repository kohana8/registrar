const path = require('path');
const {K8, ControllerMixin} = require('@komino/k8');
const fs = require('fs');
const Database = require('better-sqlite3');
const url = require('url');
const DB = {pool : []};

class ControllerMixinDatabase extends ControllerMixin{
  constructor(client, databaseFolder) {
    super(client);
    //guard hostname not registered.

    const conn = ControllerMixinDatabase.getConnections(databaseFolder);
    this.addBehaviour('databases', conn);
    this.addBehaviour('db', conn.admin);
  }

  static getConnections(dbPath){
    if(!K8.config.cache.database || !DB.pool['default']){

      DB.pool['default'] = {
        admin       : new Database(dbPath + '/admin.sqlite'),
        merchant    : new Database(dbPath + '/merchants.sqlite'),
        access_at   : Date.now()
      };
    }

    const connection = DB.pool['default'];
    connection.access_at = Date.now();
    return connection;
  }
}

module.exports = ControllerMixinDatabase;