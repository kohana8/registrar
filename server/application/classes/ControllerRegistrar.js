const {K8, Controller} = require('@komino/k8');
const {ControllerMixinMime, ControllerMixinView, ControllerMixinMultipartForm} = require('@komino/komod-mixin-orm');
const {ControllerMixinLoginRequire} = require('@komino/komod-mixin-admin');
const ControllerMixinDatabase = K8.require('controller-mixins/Database');
const Merchant = K8.require('models/Merchant');

class ControllerRegistrar extends Controller{
  constructor(request) {
    super(request);

    this.addMixin(new ControllerMixinLoginRequire(this, '/'))
    //provide mime with request path
    this.addMixin(new ControllerMixinMime(this));
    //load default.liquid
    this.addMixin(new ControllerMixinView(this));

    //enable database
    this.addMixin(new ControllerMixinDatabase(this, K8.APP_PATH+'/../../db/'));

    //accept post data
    this.addMixin(new ControllerMixinMultipartForm(this));

    const merchantDatabase = this.mixin.get('databases').merchant;
    this.merchant = Object.assign(
      new Merchant(null, {database: merchantDatabase}),
      merchantDatabase.prepare('SELECT * FROM merchants WHERE user_id = ?').get(this.request.session.user_id)
    );
  }
}

module.exports = ControllerRegistrar;