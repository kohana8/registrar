const {K8} = require('@komino/k8');
const ORM = K8.require('ORM');

class Session extends ORM{
  constructor(id, options) {
    super(id, options);
    if(id)return;

    //foreignKeys


    //fields
    this.sid = null;
    this.expired = 0;
    this.sess = null;
  }
}

Session.jointTablePrefix = 'session';
Session.tableName = 'sessions';

Session.fields = new Map([
["sid", "String!"],
["expired", "Int!"],
["sess", "String"]
]);

Session.belongsTo = new Map([

]);

Session.hasMany = [

];

Session.belongsToMany = [

];

module.exports = Session;
