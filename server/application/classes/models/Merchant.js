const {K8} = require('@komino/k8');
const ORM = K8.require('ORM');

class Merchant extends ORM{
  constructor(id, options) {
    super(id, options);
    if(id)return;

    //foreignKeys
    this.user_id = null;

    //fields
    this.identifier = null;
  }
}

Merchant.jointTablePrefix = 'merchant';
Merchant.tableName = 'merchants';

Merchant.fields = new Map([
["identifier", "String!"]
]);

Merchant.belongsTo = new Map([
["user_id", "ProxyUser"]
]);

Merchant.hasMany = [
["merchant_id", "Store"]
];

Merchant.belongsToMany = [

];

module.exports = Merchant;
