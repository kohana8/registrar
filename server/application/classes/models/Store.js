const {K8} = require('@komino/k8');
const ORM = K8.require('ORM');

class Store extends ORM{
  constructor(id, options) {
    super(id, options);
    if(id)return;

    //foreignKeys
    this.merchant_id = null;

    //fields
    this.name = null;
    this.url = null;
  }
}

Store.jointTablePrefix = 'store';
Store.tableName = 'stores';

Store.fields = new Map([
["name", "String!"],
["url", "String!"]
]);

Store.belongsTo = new Map([
["merchant_id", "Merchant"]
]);

Store.hasMany = [

];

Store.belongsToMany = [

];

module.exports = Store;
