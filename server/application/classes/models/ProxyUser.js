const {K8} = require('@komino/k8');
const ORM = K8.require('ORM');

class ProxyUser extends ORM{
  constructor(id, options) {
    super(id, options);
    if(id)return;

    //foreignKeys


    //fields
    this.key = null;
  }
}

ProxyUser.jointTablePrefix = 'proxy_user';
ProxyUser.tableName = 'proxy_users';

ProxyUser.fields = new Map([
["key", "Int"]
]);

ProxyUser.belongsTo = new Map([

]);

ProxyUser.hasMany = [
["user_id", "Merchant"]
];

ProxyUser.belongsToMany = [

];

module.exports = ProxyUser;
