const {K8} = require('@komino/k8');
const ORM = K8.require('ORM');

class Person extends ORM{
  constructor(id, options) {
    super(id, options);
    if(id)return;

    //foreignKeys


    //fields
    this.first_name = null;
    this.last_name = null;
    this.phone = null;
    this.email = null;
  }
}

Person.jointTablePrefix = 'person';
Person.tableName = 'persons';

Person.fields = new Map([
["first_name", "String!"],
["last_name", "String!"],
["phone", "String"],
["email", "String"]
]);

Person.belongsTo = new Map([

]);

Person.hasMany = [
["person_id", "User"]
];

Person.belongsToMany = [

];

module.exports = Person;
