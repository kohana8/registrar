const {K8} = require('@komino/k8');
const ORM = K8.require('ORM');

class User extends ORM{
  constructor(id, options) {
    super(id, options);
    if(id)return;

    //foreignKeys
    this.person_id = null;
    this.role_id = null;

    //fields
    this.username = null;
    this.password = null;
  }
}

User.jointTablePrefix = 'user';
User.tableName = 'users';

User.fields = new Map([
["username", "String!"],
["password", "String!"]
]);

User.belongsTo = new Map([
["person_id", "Person"],
["role_id", "Role"]
]);

User.hasMany = [

];

User.belongsToMany = [

];

module.exports = User;
