const crypto = require('crypto');
const {K8, Controller} = require('@komino/k8');
const {ControllerMixinMultipartForm} = require('@komino/komod-mixin-orm');
const ControllerMixinDatabase = K8.require('controller-mixins/Database');
const User = K8.require('models/User');
const ProxyUser = K8.require('models/ProxyUser');
const Person = K8.require('models/Person');
const Merchant = K8.require('models/Merchant');
const {Auth} = require('@komino/komod-mixin-admin');

class ControllerRegistrar extends Controller{
  constructor(client) {
    super(client);
    this.addMixin(new ControllerMixinMultipartForm(this));
    this.addMixin(new ControllerMixinDatabase(this, K8.APP_PATH+'/../../db/'));
    Auth.session = this.request.session;
  }

  async action_logout(){
    Auth.logout();
    this.redirect('/');
  }

  async action_signup(){
    const adminDB = this.mixin.get('databases').admin;
    const merchantDB = this.mixin.get('databases').merchant;
    const $_POST = this.mixin.get('$_POST');
    const identifier = $_POST['email'];
    const existUser = adminDB.prepare('SELECT id FROM users WHERE username = ?').get(identifier);
    if(!!existUser){
      this.redirect('/?already-registered');
      return;
    }

    const username = $_POST['email'];
    const password = $_POST['password'];
    const email = $_POST['email'];
    const salt = K8.config.salt;
    const hash = crypto.createHash('sha512');
    hash.update(username + password + salt);
    const hashPassword = '#' + hash.digest('hex');

    const person = new Person(null, {database: adminDB});
    person.first_name = $_POST['first_name'];
    person.last_name = $_POST['last_name'];
    person.email = email;
    person.save();

    const user = new User(null, {database: adminDB});
    user.person_id = person.id;
    user.role_id = 2; // merchant
    user.username = email;
    user.password = hashPassword;
    user.save();

    new ProxyUser(null, {createWithId:user.id ,database: merchantDB}).save();

    const merchant = new Merchant(null, {database: merchantDB});
    merchant.identifier = `email: ${$_POST['email']}`;
    merchant.user_id = user.id;
    merchant.save();

    Auth.authorize(username, password, adminDB, K8.config.salt);

    this.redirect('/merchants/dashboard')
  }
}

module.exports = ControllerRegistrar;