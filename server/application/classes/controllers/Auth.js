/* Controller auth handle login, logout*/
const path = require('path');
const {K8, Controller} = require('@komino/k8');
const {ControllerMixinMime, ControllerMixinView, ControllerMixinMultipartForm} = require('@komino/komod-mixin-orm');
const {Auth} = require('@komino/komod-mixin-admin');
const ControllerMixinDataBase = K8.require('controller-mixins/Database');

class ControllerAuth extends Controller{
  constructor(request) {
    super(request);

    this.addMixin(new ControllerMixinMime(this));
    this.addMixin(new ControllerMixinDataBase(this, path.normalize(`${K8.APP_PATH}/../../db`)));
    this.addMixin(new ControllerMixinView(this));
    this.addMixin(new ControllerMixinMultipartForm(this));

    Auth.session = this.request.session;
  }

  async action_login(){
    this.tpl = this.getView('templates/login', {
      destination : this.request.query.cp,
      message : '',
    });
  }

  async action_logout(){
    Auth.logout();

    this.tpl = this.getView('templates/login', {
      destination : '/',
      message : 'User Log Out Successfully.',
    });
  }

  async action_fail(){
    this.tpl = this.getView('templates/login', {
      destination : this.request.query.cp,
      message : 'Login fail.',
    });
  }

  async action_auth(){
    const $_POST      = this.mixin.get('$_POST');
    const destination = (!$_POST['destination'] || $_POST['destination'] === '')? '/merchants/dashboard' : $_POST['destination'];

    //guard empty post data;
    if(!$_POST['user'] || !$_POST['password'] || $_POST['user'] === '' || $_POST['password'] === ''){
      this.redirect(`/login/fail?cp=${encodeURIComponent(destination)}`);
      return;
    }

    const user = Auth.authorize($_POST['user'], $_POST['password'], this.mixin.get('databases').admin, K8.config.salt);

    if(!user){
      this.redirect(`/login/fail?cp=${encodeURIComponent(destination)}`);
      return;
    }

    this.redirect(destination);
  }

}

module.exports = ControllerAuth;
