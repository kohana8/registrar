const {K8} = require('@komino/k8');
const ControllerRegistrar = K8.require('ControllerRegistrar');

const Store = K8.require('models/Store');

class ControllerMerchant extends ControllerRegistrar{
  async action_index() {
    this.tpl = this.mixin.get('getView')('/templates/merchant/dashboard');
    const stores = this.merchant.hasMany(Store);
    Object.assign(this.tpl.data, {
      stores: stores,
    });
  }
}

module.exports = ControllerMerchant