const {K8} = require('@komino/k8');
const ControllerRegistrar = require('../ControllerRegistrar');
const Store = K8.require('models/Store');
const {ncp} = require('ncp');

class ControllerStore extends ControllerRegistrar{
  async action_create(){
    const $_POST = this.mixin.get('$_POST');
    const url = $_POST['url'];
    const merchantDB = this.mixin.get('databases').merchant;
    const existStore = merchantDB.prepare('SELECT id FROM stores WHERE url = ?').get(url);
    if(!!existStore)throw new Error(`store url "${url}" already registered`);

    const store = new Store(null, { database: merchantDB });
    store.name = $_POST['name'];
    store.url = url;
    store.merchant_id = this.merchant.id;
    store.save();

    //copy default folder to site
    const src = K8.APP_PATH + '/../../../default/site'
    const dest= K8.APP_PATH + '/../../../sites/' + url +'.shop-steam.com'
    await ncp(src, dest);

    this.redirect('/merchants/dashboard');
  }
}

module.exports = ControllerStore