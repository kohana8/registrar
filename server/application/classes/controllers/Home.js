const {Controller} = require('@komino/k8');
const {ControllerMixinMime, ControllerMixinView} = require('@komino/komod-mixin-orm');


class ControllerHome extends Controller{
  constructor(request) {
    super(request);
    //provide mime with request path
    this.addMixin(new ControllerMixinMime(this));
    //load default.liquid
    this.addMixin(new ControllerMixinView(this));
  }

  async action_index() {
    this.tpl = this.mixin.get('getView')('templates/home');
  }
}

module.exports = ControllerHome;