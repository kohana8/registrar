const {RouteList} = require('@komino/komod-route');

RouteList.add('/', 'controllers/Home')
RouteList.add('/login', 'controllers/Auth', 'auth', RouteList.methods.POST);
RouteList.add('/login/fail', 'controllers/Auth', 'fail');
RouteList.add('/logout', 'controllers/Auth', 'logout');
RouteList.add('/merchants/dashboard', 'controllers/Merchant', 'index');

RouteList.add('/signup', 'controllers/Registrar', 'signup', RouteList.methods.POST);
RouteList.add('/stores/new', 'controllers/Store', 'create', RouteList.methods.POST);