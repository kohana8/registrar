module.exports = new Map([
  ['Persons',     [{id: 1, first_name: 'John', last_name: 'Doe'}]],
  ['Roles',       [{id: 1, name: 'staff'}, {id: 2, name: 'merchant'}]],
  ['Users',       [{id: 1 ,person_id: 1, role_id: 1, username: 'admin', password: '#482b60801c7d2e841175a68e256746b0340f4d50764059c3ce2a458cc19474dd91931c737bafeeddc1cc7f98e71b9f8002e8c8ffa96ceccf1ed40d95df557aab'}]],
]);