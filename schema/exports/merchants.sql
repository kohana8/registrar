
CREATE TABLE proxy_users(
    id INTEGER UNIQUE DEFAULT ((( strftime('%s','now') - 1563741060 ) * 100000) + (RANDOM() & 65535)) NOT NULL ,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL ,
    updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL ,
    key INTEGER
);
CREATE TRIGGER proxy_users_updated_at AFTER UPDATE ON proxy_users WHEN old.updated_at < CURRENT_TIMESTAMP BEGIN
    UPDATE proxy_users SET updated_at = CURRENT_TIMESTAMP WHERE id = old.id;
END;


CREATE TABLE merchants(
    id INTEGER UNIQUE DEFAULT ((( strftime('%s','now') - 1563741060 ) * 100000) + (RANDOM() & 65535)) NOT NULL ,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL ,
    updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL ,
    identifier TEXT NOT NULL ,
    user_id INTEGER NOT NULL ,
    FOREIGN KEY (user_id) REFERENCES proxy_users (id) ON DELETE CASCADE
);
CREATE TRIGGER merchants_updated_at AFTER UPDATE ON merchants WHEN old.updated_at < CURRENT_TIMESTAMP BEGIN
    UPDATE merchants SET updated_at = CURRENT_TIMESTAMP WHERE id = old.id;
END;


CREATE TABLE stores(
    id INTEGER UNIQUE DEFAULT ((( strftime('%s','now') - 1563741060 ) * 100000) + (RANDOM() & 65535)) NOT NULL ,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL ,
    updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL ,
    name TEXT NOT NULL ,
    url TEXT NOT NULL ,
    merchant_id INTEGER NOT NULL ,
    FOREIGN KEY (merchant_id) REFERENCES merchants (id) ON DELETE CASCADE
);
CREATE TRIGGER stores_updated_at AFTER UPDATE ON stores WHEN old.updated_at < CURRENT_TIMESTAMP BEGIN
    UPDATE stores SET updated_at = CURRENT_TIMESTAMP WHERE id = old.id;
END;
