const {build} = require('@komino/k8-scaffold');
build(
  `${__dirname}/admin.graphql`,
  `${__dirname}/admin.js`,
  `${__dirname}/exports/admin.sql`,
  `${__dirname}/../db/admin.sqlite`,
  `${__dirname}/../server/application/classes/models`
)

build(
  `${__dirname}/sessions.graphql`,
  ``,
  `${__dirname}/exports/sessions.sql`,
  `${__dirname}/../db/sessions.sqlite`,
  `${__dirname}/../server/application/classes/models`
)

build(
  `${__dirname}/merchant.graphql`,
  ``,
  `${__dirname}/exports/merchants.sql`,
  `${__dirname}/../db/merchants.sqlite`,
  `${__dirname}/../server/application/classes/models`
)